$(function(){
    $(".lisaSound").submit(function(){
        event.preventDefault();
        console.log("joudis javascripti");
        $('.response').empty();

        var fd = new FormData();
        fd.append('file', $('#formAddSoundFile')[0].files[0]);
        fd.append('name', $('#formAddSound').val());
        var formdata = new FormData($(this)[0]);
        $.ajax({
            url: '/addSound',
            type: 'POST',
            //data: $('form').serialize(),
            data: formdata,
            contentType: false,
            cache: false,
            processData: false,
            async: false,
            success: function(response){
                console.log(response);
                var result = $.trim(response);
                console.log(response);
                if(response==='{"html": "<span>Sama nimi</span>"}'){
                    $('#response').append("<h4>Selline sõna on juba olemas, kas soovid seda muuta?</h4>");
                    console.log("ei ole vaba");
                   // $('.response').empty();
                    updateSound();
                }
                else if(response==='{"html": "<span>Enter the required fields</span>"}'){
                    $('#response').append("<h4>Täida kõik väljad!</h4>");
                }
                else {
                    $('#response').append("<h4>Sõna lisatud!</h4>");
                }
            },
            error: function(error) {
                console.log(error);
            }
        });
    });
});


$(function(){
    $(".updateSound").submit(function(){
        event.preventDefault();
        $('.response').empty();
        console.log("joudis javascripti");
        var fd = new FormData();

        fd.append('file', $('#formAddSoundFile')[0].files[0]);
        fd.append('name', $('#formAddSound').val());
        var formdata = new FormData($(this)[0]);
        $.ajax({
            url: '/updateSound',
            type: 'POST',
            //data: $('form').serialize(),
            data: formdata,
            contentType: false,
            cache: false,
            processData: false,
            async: false,
            success: function(response){
                console.log(response);
                $('#response').append("<h4>Sõna lisatud!</h4>");
            },
            error: function(error) {
                console.log(error);
            }
        });
        hideButtons();
    });
});

function updateSound(){
    document.getElementById("updateButtons").style.display = "inline";
}

function hideButtons(){
    document.getElementById("updateButtons").style.display = "none";
 //   $('.response').empty();
}

$(function(){
    $('.kuvaSound').submit(function(){

        if($('#formGetSound').val() != ''){
            document.getElementById("helifail").style.visibility = "visible"
        }

        event.preventDefault();
        $('.vastused').empty();
        console.log("kuvamise skript");
        var formdata = new FormData($(this)[0]);
        $.ajax({
            url: '/getSound',
            type: 'POST',
            data: $('form').serialize(),
            success: function(res){
                console.log(res);
                $('#audiofail').remove();
                if (res==='{"error": "\'NoneType\' object is not subscriptable"}'){
                    $('#vastused').append("<h4>Sellist sõna veel pole! Saad selle teisel lehel lisada.</h4>");
                }
                else {
                $('#helifail').append('<audio controls id="audiofail"><source src="../static/' + JSON.parse(res) + '.mp3" type="audio/mpeg"></audio>');
                var div = $('<div>')
                    .attr('class', 'list-group')
                    .append($('<a>')
                        .attr('class', 'list-group-item active')
                        .append($('<h4>')
                            .attr('class', 'list-group-item-heading'),
                            $('<p>')
                            .attr('class', 'list-group-item-text')));


                $('#vastused').append("<h4>Sinu sõna:</h4>");
				var wishObj = JSON.parse(res);
			//	console.log(wishObj.nimi);
				$('#vastused').append(wishObj);
				var wish = '';
				}
			},
			error: function(error){
				console.log(error);
			}

        });

    });
});

var inputBox = document.getElementById('formGetSound');
var available = [];
function getAllSounds(){
    if(this.value!=""){

        $.ajax({
                url: '/getAllSounds',
                type: 'GET',
                contentType: false,
                cache: false,
                processData: false,
                async: false,
                success: function(response){
                    available.length = 0;

                    var array = JSON.parse(response);
                    available = array;
                    //console.log(available);
                    $( "#formGetSound" ).autocomplete({
                        source: available
                    });

                },
                error: function(error) {
                    console.log(error);
                }
            });
}
}
