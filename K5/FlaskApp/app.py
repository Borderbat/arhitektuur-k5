from flask import Flask, render_template, json, request, request, redirect, url_for
from flaskext.mysql import MySQL
from werkzeug import generate_password_hash, check_password_hash
from werkzeug.utils import secure_filename
import pymongo
from pymongo import MongoClient
import gridfs
from flask import make_response
from bson.objectid import ObjectId
import os

# mysql = MySQL()
app = Flask(__name__)

# MySQL configurations
# app.config['MYSQL_DATABASE_USER'] = 'root'
# app.config['MYSQL_DATABASE_PASSWORD'] = ''
# app.config['MYSQL_DATABASE_DB'] = 'Harjutus1'
# app.config['MYSQL_DATABASE_HOST'] = 'localhost'
# mysql.init_app(app)

client = pymongo.MongoClient("mongodb://localhost:27017")
db = client.sounds
soundList = db.sounds_collection

fs = gridfs.GridFS(db)
_sound = ""
_soundFile = None
_vanaSound = ""
updatefile = None

@app.route("/")
def main():
    return render_template('getSound.html')

@app.route('/addSoundPage', methods=['GET', 'POST'])
def addSoundPage():
    return render_template('addSound.html')

@app.route('/getSoundPage')
def getSoundPage():
    return render_template('getSound.html')

@app.route('/getSound', methods=['GET', 'POST'])
def getSound():
    try:
        global _vanaSound
        #_vanaSound = "sound"
        print(_vanaSound)
        if _vanaSound != "":
            os.remove("static/" + _vanaSound + ".mp3")
        print("enne sound")
        _sound = request.form['formGetSound']
        print("pärast sound")

        # validate the received values
        if _sound:
            print("if lause")
            vastus = soundList.find_one({
                "nimi": _sound
            })
            a = soundList.find_one({'nimi':_sound})

            print("printimine")
            print (a)
            var1 = a['soundFile']
            #print(fs.exists(var1))

            #f = open('static/sound2.mp3', 'wb')
            f = open('static/'+ _sound + '.mp3', 'wb')
            #f.write(fs.get(file).read())
            f.write(fs.get(a['soundFile']).read())
            f.close()

            #with fs.get(var1) as fp_read:
           #     f = open('static/sound.mp3', 'wb')
            #    f.write(fp_read.read())
             #   f.close()
            _vanaSound = _sound
            print(vastus['nimi'])
            return json.dumps(vastus['nimi'])

        else:
            return json.dumps({'html': '<span>Enter the required fields</span>'})

    except Exception as e:
        return json.dumps({'error': str(e)})


@app.route('/addSound', methods=['GET', 'POST'])
def addSound():
    try:
        global _sound
        global _soundFile
        global updatefile
        if updatefile:
            fs.delete(updatefile)
        print(_soundFile)
        print(_sound)
        if request.method == 'POST':
            print("joudis kohale pythonisse");
            _sound = request.form['formAddSound']
            print("nimi")
            _soundFile = request.files['formAddSoundFile']
            print(_soundFile)
            print("fail")

            samaNimi = soundList.find_one({
                "nimi": _sound
            })

            print(samaNimi)
            #kontrollib kas nimi on juba olemas
            if samaNimi and len(samaNimi) > 0:
                updatefile = fs.put(_soundFile)
                return json.dumps({'html': '<span>Sama nimi</span>'})
            print("peale kontrolli")

            # validate the received values
            if _sound and _soundFile:
                file = fs.put(_soundFile)
                sound = {"nimi": _sound,
                         "soundFile": file }
                soundList.insert_one(sound)
                print("if lauses2")
                print(fs.get(file).read())
                #print(make_response(fs.get(file).read()))
                #response = make_response(fs.get(file).read())
                #response.mimetype = 'image/jpeg'
                #return response

                return json.dumps({'html': '<span>Success</span>'})

            else:
                return json.dumps({'html': '<span>Enter the required fields</span>'})

    except Exception as e:
        return json.dumps({'error': str(e)})


@app.route('/updateSound', methods=['GET','POST'])
def updateSound():
    try:
        global _sound
        global updatefile
        #global _soundFile
        if request.method == 'POST':
            print("on updates")
            #_sound = request.form['formAddSound']
            print(_sound) #ei saa k2tte
            #_soundFile = request.files['formAddSoundFile']
            #print(_soundFile)
            print(updatefile)
            print("fail")

            # validate the received values
            if _sound and updatefile:
                print("update if lauses")
                #file = fs.put(_soundFile)
                print("update if lauses4")
                sound = {"nimi": _sound,
                         "soundFile": updatefile}
                print("update if lauses2")
                soundList.update({
                    'nimi': _sound
                },{
                    '$set': {
                        'soundFile': updatefile
                    }
                })
                print("update if lauses3")
               # soundList.update(sound)
                print("if lauses2")
                #print(fs.get(file).read())
                updatefile = None

                return json.dumps({'html': '<span>Success</span>'})

         #   soundList.update({
          #      'nimi': _sound
          #  },{
           #     '$set': {
            #        'soundFile': _soundFile
             #   }
            #})

            else:
                return json.dumps({'html': '<span>Enter the required fields</span>'})

    except Exception as e:
        return json.dumps({'error': str(e)})


@app.route('/getAllSounds', methods=['GET', 'POST'])
def getAllSounds():
    try:
      #  print("enne sound")
        soundList.distinct('nimi')
        print(soundList.distinct('nimi'))

        return json.dumps(soundList.distinct('nimi'))

    except Exception as e:
        return json.dumps({'error': str(e)})




if __name__ == "__main__":
    app.run(port=5023)
